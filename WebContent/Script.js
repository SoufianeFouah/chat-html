/**
 * 
 */

$(document).ready(
	function(){
		$("#btnLogin").click(
			function(){
				let varusername= $("#inputUsername").val();	
				let varpassword= $("#inputPassword").val();	

				$.ajax(
					{
						url:"http://localhost:8081/RealtameChat/verifica/login",
						contentType:"application/json",
						method: "POST",
						data: JSON.stringify({
							username:varusername,
							passwordUtente:varpassword
						}),
						
						success:function(resposta){
							if(resposta != undefined){
								if(resposta.tipologia == "Admin"){
									window.location.href = "Admin.html";
								}
								else{
									window.location.href = "UtenteBase.html";
								}
							}
						},
						error:function(error){
							alert("errore " + error);
						}
					}
					
				);

			}	
						
		);	
	}
);